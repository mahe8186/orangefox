import Link, { LinkProps } from 'next/link';

type AnchorLinkProps = Partial<LinkProps> & {
  activeClassName?: string;
  children?: React.ReactNode;
  ATagProps?: React.AnchorHTMLAttributes<HTMLAnchorElement>;
};

const AnchorLink = ({
  as,
  href,
  children,
  ATagProps,
  ...props
}: AnchorLinkProps) => {
  return (
    <Link href={href || '/'} as={as || '/'} {...props} {...ATagProps}>
      {children}
    </Link>
  );
};

export { AnchorLink };
